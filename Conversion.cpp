//Gilberto E.Cancel Morales
//CCOM 3033-001


#include<iostream>
#include<string>


using namespace std;

float inches, centimeters, inch_meters, meters_inch,

      feet, meters, feet_meter, meter_feet,

      miles, kilometers, miles_kilom, kilom_miles,

      ounces, gram, ounces_gram, gram_ounces,

      pound, kilogram, pound_kilog, kilog_pound,

      ounces2, mililiters, ounce_mili, mili_ounce,

      galon, liter, galon_liter, liter_galon,

      cuartillo, liter2, cuar_liter, liter_cuar,

      psi, kg, psi_kg, kg_psi,

      fahr, cel, fahr_cel, cel_fahr;

string conversion;

int main()
{

cout << "Write the conversion you would like: " << endl <<
    endl << "Length: inches to centimeters (Write inches); centimeters to inches (Write centi)" << endl <<

    endl << "Length: feet to meters (Write feet); meters to feet (Write meters)" << endl <<

    endl << "Length: miles to kilometers (Write miles); kilometers to miles (Write kilom)" << endl <<

    endl << "Weight: ounces to grams (Write wounce); grams to ounces (Write gram)" << endl <<

    endl << "Weight: pounds to kilograms (Write pound); kilograms to pounds (Write kilog)" << endl <<

    endl << "Volume: ounces to milliliters (Write vounce); milliliters to ounces (Write mili)" << endl <<

    endl << "Volume: galon to liters (Write galon); liters to galon (Write liter)" << endl <<

    endl << "Volume: quadrille to liters (Write quad); liters to quadrille (Write liter2)" << endl <<

    endl << "Pressure: psi to kilogram/cm (Write psi); kilogram/cm to psi (Write kgcm)" << endl <<

    endl << "Temperature: fahrenheit to celsius (Write fahrenheit); celsius to farenheit (Write celsius)" << endl << endl;

    cin >> conversion;

//Inches and Centimeters
if (conversion == "inches")
    {
    cout << "How many inches would you like to convert?" << endl;
        cin >> inches;

     inch_meters  = inches * 2.54;

     cout << inch_meters << " centimeters" << endl;
    }
else if (conversion == "centi")
    {
    cout << "How many centimeters would you like to convert?"<< endl;
        cin >> centimeters;

    meters_inch = centimeters / 2.54;

    cout << meters_inch << " inches" << endl;
    }


//Feet and Meters
if (conversion == "feet")
    {
    cout << "What distance in feet would you like to convert?" << endl;
        cin >> feet;

     feet_meter = feet / 3.281;

     cout << feet_meter << " meters" << endl;
    }
else if (conversion == "meters")
    {
    cout << "What distance in meters would you like to convert?"<< endl;
        cin >> meters;

    meter_feet  = meters * 3.281;

    cout << meter_feet << " feet" << endl;
    }

//Miles and Kilometers
else if (conversion == "miles")
    {
    cout << "How many miles would you like to convert?" << endl;
        cin >> miles;

     miles_kilom = miles * 1.609;

     cout << miles_kilom << " kilometers" << endl;
    }
else if (conversion == "kilom")
    {
    cout << "How many kilometers would you like to convert?"<< endl;
        cin >> kilometers;

    kilom_miles = kilometers / 1.609;

    cout << kilom_miles << " miles" << endl;
    }


//Ounces and Grams
else if (conversion == "wounce")
    {
    cout << "How many ounces would you like to convert?" << endl;
        cin >> ounces;

     ounces_gram = ounces * 28.35;

     cout << ounces_gram << " grams" << endl;
    }
else if (conversion == "gram")
    {
    cout << "How many grams would you like to convert?"<< endl;
        cin >> gram;

    gram_ounces = gram / 28.35;

    cout << gram_ounces << " ounces" << endl;
    }


//Pounds and Kilograms
else if (conversion == "pound")
    {
    cout << "How many pounds would you like to convert?" << endl;
        cin >> pound;

     pound_kilog = pound / 2.205;

     cout << pound_kilog << " kilograms" << endl;
    }
else if (conversion == "kilog")
    {
    cout << "How many kilograms would you like to convert?"<< endl;
        cin >> kilogram;

    kilog_pound = kilogram * 2.205;

    cout << kilog_pound << " pounds" << endl;
    }


//Ounces(volume) and Milliliters
else if (conversion == "vounce")
    {
    cout << "How many ounces would you like to convert?" << endl;
        cin >> ounces2;

     ounce_mili = ounces2 * 29.574;

     cout << ounce_mili << " milliliters" << endl;
    }
else if (conversion == "mili")
    {
    cout << "How many milliliters would you like to convert?"<< endl;
        cin >> mililiters;

    mili_ounce = mililiters / 29.574;

    cout << mili_ounce << " ounces" << endl;
    }


//Galon and Liters
else if (conversion == "galon")
    {
    cout << "How many galons would you like to convert?" << endl;
        cin >> galon;

     galon_liter = galon * 3.785;

     cout << galon_liter << " liters" << endl;
    }
else if (conversion == "liter")
    {
    cout << "Cuantos liters vas a convertir?"<< endl;
        cin >> liter;

    liter_galon = liter / 3.785;

    cout << liter_galon << " galons" << endl;
    }


//Quadrilles and Liters
else if (conversion == "quad")
    {
    cout << "How many quadrilles would you like to convert?" << endl;
        cin >> cuartillo;

     cuar_liter = cuartillo / 2.113;

     cout << cuar_liter << " liters" << endl;
    }
else if (conversion == "liter2")
    {
    cout << "How many liters would you like to convert?"<< endl;
        cin >> liter;

    liter_cuar = liter * 2.113;

    cout << liter_cuar << " quadrille" << endl;
    }



//PSI and kg/cm
else if (conversion == "psi")
    {
    cout << "How much psi would you like to convert?" << endl;
        cin >> psi;

     psi_kg = psi / 14.223;

     cout << psi_kg << " kg/cm" << endl;
    }
else if (conversion == "kgcm")
    {
    cout << "How much kg/cm would you like to convert??"<< endl;
        cin >> kg;

    kg_psi = kg * 14.223;

    cout << kg_psi << "PSI" << endl;
    }



//Fahrenheit and Celsius
else if (conversion == "fahrenheit")
    {
    cout << "What temperature would you like to change from farenheit?" << endl;
        cin >> fahr;

     fahr_cel = (fahr-32) * 5/9;

     cout << fahr_cel << " celsius" << endl;
    }
else if (conversion == "celsius")
    {
    cout << "What temperature would you like to change from celsius?"<< endl;
        cin >> cel;

    cel_fahr = (cel * 9/5) + 32;

    cout << cel_fahr << " fahrenheit" << endl;
    }


return 0;
}
